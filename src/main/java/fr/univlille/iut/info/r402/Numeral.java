package fr.univlille.iut.info.r402;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Numeral {
    private Map<String, String> sampleNumbers = new HashMap<>();
    private Map<String, String> dizaineComplexNumbers = new HashMap<>();
    private Map<String, String> dizaineNumbers = new HashMap<>();
    private String number;

    public Numeral(String number) {
        List<String> sampleNumbers = List.of("zéro", "un", "deux", "trois", "quatre", "cinq", "six", "sept", "huit", "neuf");
        
        for (int i = 0; i < 10; i++) 
            this.sampleNumbers.put(String.valueOf(i), sampleNumbers.get(i));
        
        List<String> dizaineComplexNumbers = List.of("onze", "douze", "treize", "quatorze", "quinze", "seize");
        
        for (int i = 0; i < 6; i++) 
            this.dizaineComplexNumbers.put(String.valueOf(11+i), dizaineComplexNumbers.get(i));
        
        List<String> dizaineNumbers = List.of("dix", "vingt", "trente", "quarante", "cinquante", "soixante", "soixante-dix", "quatre-vingt", "quatre-vingt-dix");

        for (int i = 0; i < 9; i++) 
            this.dizaineNumbers.put(String.valueOf((1+i) * 10), dizaineNumbers.get(i));
        
        this.number = number;
    }

    public String toLetters() {
        Integer numberNum = Integer.parseInt(this.number);
        StringBuilder result = new StringBuilder();
        if (number.length() == 1) {
            return capitalizeFirstLetter(sampleNumbers.get(number));
        } else if (number.length() == 2) {
            if (numberNum < 17 && numberNum > 10) {
                return capitalizeFirstLetter(dizaineComplexNumbers.get(number));
            }
            String dizaine = number.charAt(0) + "0";
            result.append(dizaineNumbers.get(dizaine));
            if (number.charAt(1) != '0') {
                result.append("-");
                result.append(sampleNumbers.get(String.valueOf(number.charAt(1))));
            }
            if (result.toString().equals("vingt-un"))
                result.replace(5, 6, " et ");
            
            return capitalizeFirstLetter(result.toString());
        }
        return null;
    }

    private String capitalizeFirstLetter(String word) {
        return word.substring(0, 1).toUpperCase() + word.substring(1);
    }

    public static void main(String[] args) {
        Numeral one = new Numeral("1");
        System.out.println(one.toLetters());
        Numeral seven = new Numeral("7");
        System.out.println(seven.toLetters());
        Numeral ten = new Numeral("10");
        System.out.println(ten.toLetters());
        Numeral eighteen = new Numeral("18");
        System.out.println(eighteen.toLetters());
        Numeral dsq = new Numeral("25");
        System.out.println(dsq.toLetters());
    }
}
