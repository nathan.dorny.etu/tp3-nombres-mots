package fr.univlille.iut.info.r402;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class NumeralTest {
    @Test
    public void testNumeral() {
        Numeral one = new Numeral("1");
        assertEquals("Un", one.toLetters());
        Numeral seven = new Numeral("7");
        assertEquals("Sept", seven.toLetters());
    }

    @Test
    public void testNumeralWithTwoNumbers() {
        Numeral dix = new Numeral("10");
        assertEquals("Dix", dix.toLetters());
        Numeral dixHuit = new Numeral("18");
        assertEquals("Dix-huit", dixHuit.toLetters());
    }

    @Test
    public void testNumeralWithTwoNumbersComplexe() {
        Numeral dix = new Numeral("11");
        assertEquals("Onze", dix.toLetters());
        Numeral dixHuit = new Numeral("12");
        assertEquals("Douze", dixHuit.toLetters());
    }

    @Test
    public void testNumeralWithTwoNumbersSecondDizaine() {
        Numeral vingt = new Numeral("20");
        assertEquals("Vingt", vingt.toLetters());
        Numeral vingtEtUn = new Numeral("21");
        assertEquals("Vingt et un", vingtEtUn.toLetters());
        Numeral vingtHuit = new Numeral("28");
        assertEquals("Vingt-huit", vingtHuit.toLetters());
    }

    @Test
    public void testNumeralWithTwoNumbersAutreDizaines() {
        Numeral trenteNeuf = new Numeral("39");
        assertEquals("Trente-neuf", trenteNeuf.toLetters());
    }

    @Test
    public void testNumeralWithTwoNumbersAutreDizaines2() {
        Numeral quatreVingtSeize = new Numeral("96");
        assertEquals("Quatre-vingt-seize", quatreVingtSeize.toLetters());
    }
}